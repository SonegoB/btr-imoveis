import { useLoading } from '@/hooks/useLoading';
import { api } from '@/services/axios';
import handleErrors from '@/utils/handleErrors';
import React, {
  FormEvent, useCallback, useEffect, useState,
} from 'react';
import { toast } from 'react-toastify';
import { ufList } from '@/constants/UF';
import formatCurrencyToBrazil from '@/utils/formatCurrencyToBrazil';
import { InputSearch } from '../InputSearch';
import styles from './styles.module.scss';

interface ICategory {
    id_categoria: string;
    titulo: string;
}

interface ICostumer {
    id_proprietario: string;
    nome: string;
}

interface IModality {
    id_modalidade: string;
    titulo: string;
}

interface ICity {
    id: string;
    nome: string;
}

interface IFilter {
    id_cidade_fk?: string;
    id_categoria_fk?: string;
    rua?: string;
    bairro?: string;
    garagem?: string;
    id_proprietario_fk?: string;
    id_modalidade_fk?: string;
}

interface SearchImmobileProps {
    onHandleSearchToImmobile: (filteredImmobile: any[]) => void;
    categories?: ICategory[] | null;
    type: 'page' | 'admin';
}

export function SearchImmobile({
  onHandleSearchToImmobile,
  categories,
  type,
}: SearchImmobileProps) {
  const { setLoading } = useLoading();
  const [filter, setFilter] = useState<IFilter>({
    id_modalidade_fk: type === 'admin' ? '' : '1',
  } as IFilter);
  const [ufSelected, setUfSelected] = useState('');
  const [cities, setCities] = useState<ICity[]>([]);
  const [categoryList, setCategoryList] = useState<ICategory[]>([]);
  const [costumerList, setCosutmerList] = useState<ICostumer[]>([]);
  const [modalityList, setModalityList] = useState<IModality[]>([]);

  useEffect(() => {
    function getInformation() {
      api.post('/categoria/pesquisa', { categoria: {} }).then((response) => {
        setCategoryList(response.data.result);
      });

      api.post('/proprietario/pesquisa', { proprietario: {} }).then((response) => {
        setCosutmerList(response.data.result);
      });

      api.post('/modalidade/pesquisa', { modalidade: {} }).then((response) => {
        setModalityList(response.data.result);
      });
    }

    if (categories) {
      setCategoryList(categories);
    } else {
      getInformation();
    }
  }, [categories]);

  useEffect(() => {
    if (ufSelected !== '') {
      setLoading(true);

      api.post('/cidade/pesquisa', {
        cidade: {
          uf: ufSelected,
        },
      }).then((response) => {
        setLoading(false);
        setCities(response.data);
      }).catch(() => {
        setLoading(false);
        setCities([]);
      });
    }
  }, [ufSelected, setLoading]);

  const handleFilter = useCallback(async () => {
    if (filter.bairro === '') {
      delete filter.bairro;
    }

    if (filter.garagem === '') {
      delete filter.garagem;
    }

    if (filter.id_categoria_fk === '') {
      delete filter.id_categoria_fk;
    }

    if (filter.id_cidade_fk === '') {
      delete filter.id_cidade_fk;
    }

    if (filter.rua === '') {
      delete filter.rua;
    }

    if (filter.id_categoria_fk === '') {
      delete filter.id_categoria_fk;
    }

    if (filter.id_modalidade_fk === '') {
      delete filter.id_modalidade_fk;
    }

    if (filter.id_proprietario_fk === '') {
      delete filter.id_proprietario_fk;
    }
  }, [filter]);

  const handleGetImmobile = useCallback(async (event?: FormEvent) => {
    if (event) {
      event.preventDefault();
    }

    setLoading(true);
    try {
      handleFilter();
      const response = await api.post('/imovel/pesquisa', {
        imovel: {
          status: 'ATIVO',
          ...filter,
        },
      });

      setLoading(false);
      onHandleSearchToImmobile(response.data.result.map((immobileMapped) => ({
        ...immobileMapped,
        valorFormatado: formatCurrencyToBrazil(Number(immobileMapped.valor)),
        banner: immobileMapped.imagens.find((imageMapped) => imageMapped.destaque === 'SIM')
          ? immobileMapped.imagens.find((imageMapped) => imageMapped.destaque === 'SIM').file
          : immobileMapped.imagens[0]?.file ?? '',
      })));
    } catch (err) {
      setLoading(false);
      if (err.response) {
        const { message } = handleErrors(err.response);
        toast.error(message[0]);
      } else {
        toast.error('Erro ao fazer busca!');
      }
    }
  }, [filter, handleFilter, onHandleSearchToImmobile, setLoading]);

  const handleRemoveFilter = useCallback(async () => {
    const bairro = '';
    const garagem = '';
    const id_categoria_fk = '';
    const id_cidade_fk = '';
    const rua = '';
    const id_proprietario_fk = '';
    let id_modalidade_fk;

    if (type === 'admin') {
      id_modalidade_fk = '';
    } else {
      id_modalidade_fk = filter.id_modalidade_fk;
    }

    setFilter({
      bairro,
      garagem,
      id_categoria_fk,
      id_cidade_fk,
      rua,
      id_proprietario_fk,
      id_modalidade_fk,
    });
    setCities([]);
    setUfSelected('');
    setLoading(true);
    try {
      handleFilter();
      const response = await api.post('/imovel/pesquisa', {
        imovel: {
          status: 'ATIVO',
          id_modalidade_fk: filter.id_modalidade_fk,
        },
      });

      setLoading(false);
      onHandleSearchToImmobile(response.data.result.map((immobileMapped) => ({
        ...immobileMapped,
        valorFormatado: formatCurrencyToBrazil(Number(immobileMapped.valor)),
        banner: immobileMapped.imagens.find((imageMapped) => imageMapped.destaque === 'SIM')
          ? immobileMapped.imagens.find((imageMapped) => imageMapped.destaque === 'SIM').file
          : immobileMapped.imagens[0]?.file ?? '',
      })));
    } catch (err) {
      setLoading(false);
      if (err.response) {
        const { message } = handleErrors(err.response);
        toast.error(message[0]);
      } else {
        toast.error('Erro ao fazer busca!');
      }
    }
  }, [filter.id_modalidade_fk, handleFilter, onHandleSearchToImmobile, setLoading, type]);

  const handleNavAba = useCallback(async (modalidade: string) => {
    setFilter({ id_modalidade_fk: modalidade });
    setLoading(true);
    try {
      handleFilter();
      const response = await api.post('/imovel/pesquisa', {
        imovel: {
          status: 'ATIVO',
          ...filter,
          id_modalidade_fk: modalidade,
        },
      });

      setLoading(false);
      onHandleSearchToImmobile(response.data.result.map((immobileMapped) => ({
        ...immobileMapped,
        valorFormatado: formatCurrencyToBrazil(Number(immobileMapped.valor)),
        banner: immobileMapped.imagens.find((imageMapped) => imageMapped.destaque === 'SIM')
          ? immobileMapped.imagens.find((imageMapped) => imageMapped.destaque === 'SIM').file
          : immobileMapped.imagens[0]?.file ?? '',
      })));
    } catch (err) {
      setLoading(false);
      if (err.response) {
        const { message } = handleErrors(err.response);
        toast.error(message[0]);
      } else {
        toast.error('Erro ao fazer busca!');
      }
    }
  }, [filter, handleFilter, onHandleSearchToImmobile, setLoading]);

  return (
    <section className={styles.searchContainer}>

      <form className={styles.formContainer} onSubmit={handleGetImmobile}>
        { type === 'page'
        && (
          <>
            <h1>BTR Imóveis</h1>
            <div className={styles.navAba}>
              <button
                type="button"
                className={filter.id_modalidade_fk === '1' ? styles.active : ''}
                onClick={
              () => {
                handleNavAba('1');
              }
            }
              >
                Venda

              </button>
              <button
                type="button"
                className={filter.id_modalidade_fk === '2' ? styles.active : ''}
                onClick={
              () => {
                handleNavAba('2');
              }
            }
              >
                Aluguel

              </button>
            </div>
          </>
        )}
        <div className={styles.formContent}>
          <div>
            <label htmlFor="uf">UF</label>
            <select
              id="uf"
              value={ufSelected}
              onChange={(e) => { setUfSelected(e.target.value); }}
            >
              <option value="">Selecione</option>
              {ufList.map((ufMapped) => (
                <option key={ufMapped.value} value={ufMapped.value}>{ufMapped.label}</option>
              ))}
            </select>
            <label htmlFor="cidade">Cidade</label>
            <select
              id="cidade"
              value={filter.id_cidade_fk}
              onChange={(e) => {
                setFilter({
                  ...filter,
                  id_cidade_fk: e.target.value,
                });
              }}
            >
              <option value="">{ufSelected === '' ? 'Selecione um estado' : 'selecione'}</option>
              {cities.map((cityMapped) => (
                <option key={cityMapped.id} value={cityMapped.id}>{cityMapped.nome}</option>
              ))}
            </select>
          </div>
          <div>
            <label htmlFor="tipo">Endereço:</label>
            <InputSearch
              value={filter.rua}
              placeholder="Todos"
              onChange={(e) => {
                setFilter({
                  ...filter,
                  rua: e.target.value,
                });
              }}
            />
            <label htmlFor="bairro">Bairro:</label>
            <InputSearch
              value={filter.bairro}
              placeholder="Todos"
              onChange={(e) => {
                setFilter({
                  ...filter,
                  bairro: e.target.value,
                });
              }}
            />
          </div>
          <div>
            <label htmlFor="garagem">Garagem</label>
            <select
              id="garagem"
              value={filter.garagem}
              onChange={(e) => {
                setFilter({
                  ...filter,
                  garagem: e.target.value,
                });
              }}
            >
              <option value="">Selecione</option>
              <option value="SIM">SIM</option>
              <option value="NAO">NÃO</option>
            </select>
            <label htmlFor="categoria">Categoria</label>
            <select
              id="categoria"
              value={filter.id_categoria_fk}
              onChange={(e) => {
                setFilter({
                  ...filter,
                  id_categoria_fk: e.target.value,
                });
              }}
            >
              <option value="">Selecione</option>
              {categoryList.map((categoryMapped) => (
                <option
                  key={categoryMapped.id_categoria}
                  value={categoryMapped.id_categoria}
                >
                  {categoryMapped.titulo}

                </option>
              ))}
            </select>
          </div>
        </div>
        {type === 'admin'
        && (
        <div className={styles.filterFlexContainer}>
          <div>
            <label htmlFor="cliente">Clientes</label>
            <select
              id="cliente"
              value={filter.id_proprietario_fk}
              onChange={(e) => {
                setFilter({
                  ...filter,
                  id_proprietario_fk: e.target.value,
                });
              }}
            >
              <option value="">Selecione</option>
              {costumerList.map((categoryMapped) => (
                <option
                  key={categoryMapped.id_proprietario}
                  value={categoryMapped.id_proprietario}
                >
                  {categoryMapped.nome}

                </option>
              ))}
            </select>
          </div>
          <div>
            <label htmlFor="modalidade">Tipo</label>
            <select
              id="modalidade"
              value={filter.id_modalidade_fk}
              onChange={(e) => {
                setFilter({
                  ...filter,
                  id_modalidade_fk: e.target.value,
                });
              }}
            >
              <option value="">Selecione</option>
              {modalityList.map((modalityMapped) => (
                <option
                  key={modalityMapped.id_modalidade}
                  value={modalityMapped.id_modalidade}
                >
                  {modalityMapped.titulo}

                </option>
              ))}
            </select>
          </div>
        </div>
        )}
        <div className={styles.containerButton}>
          <button type="submit">Buscar</button>
          <button
            type="button"
            onClick={handleRemoveFilter}
          >
            Zerar filtro

          </button>
        </div>
      </form>
    </section>
  );
}
