import { FiLoader } from 'react-icons/fi';
import styles from './styles.module.scss';

export function Loading(): JSX.Element {
  return (
    <div className={styles.container}>
      <div className={styles.loadingContainer}>
        <FiLoader />
        <span>Carregando</span>
      </div>
    </div>
  );
}
