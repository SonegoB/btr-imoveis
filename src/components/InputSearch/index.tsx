import { InputHTMLAttributes } from 'react';
import styles from './styles.module.scss';

export function InputSearch({ ...rest }: InputHTMLAttributes<HTMLInputElement>): JSX.Element {
  return (
    <input className={styles.input} {...rest} />
  );
}
