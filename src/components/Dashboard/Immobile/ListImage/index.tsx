import React, {
  useCallback,
  useEffect, useState,
} from 'react';
import { api } from '@/services/axios';

import { FiArrowLeft, FiPlus } from 'react-icons/fi';
import { useLoading } from '@/hooks/useLoading';
import { toast } from 'react-toastify';
import handleErrors from '@/utils/handleErrors';
import styles from './styles.module.scss';
import { ModalImage } from './ModalImage';

interface ListImageProps {
    onHandleOptionSelected: (option: 'listar' | 'criar') => void;
    immobile: any;
    onHandleSelectAImmobile: (immobile: any) => void;
}

interface IImage {
  file: string;
  id_imovel_imagem: string;
  order: string;
  destaque: string;
  titulo: string;
}

export function ListImage({
  onHandleOptionSelected,
  immobile,
  onHandleSelectAImmobile,
}: ListImageProps): JSX.Element {
  const { setLoading } = useLoading();
  const [openModal, setOpenModal] = useState(false);
  const [images, setImages] = useState<IImage[]>([]);
  const [imageSelected, setImageSelected] = useState<IImage>({} as IImage);

  useEffect(() => {
    setLoading(true);
    api.get((`/imovel/${immobile.id_imovel}/imagem`))
      .then((response) => {
        setImages(response.data.imagens);
        setLoading(false);
      })
      .catch((err) => {
        setLoading(false);
        setImages([]);
        console.log(err);
      });
  }, [immobile, setLoading]);

  const handleCloseUpdate = useCallback(() => {
    onHandleSelectAImmobile({} as any);
    onHandleOptionSelected('listar');
  }, [onHandleOptionSelected, onHandleSelectAImmobile]);

  const handleRemoveImage = useCallback(async (id: string) => {
    setLoading(true);

    try {
      await api.delete(`imovel/${immobile.id_imovel}/imagem/${id}`);
      const findIndex = images.findIndex((imageMapped) => imageMapped.id_imovel_imagem === id);
      images.splice(findIndex, 1);
      setLoading(false);
    } catch (err) {
      if (err.response) {
        setLoading(false);
        const { message } = handleErrors(err.response);
        toast.error(message[0]);
      } else {
        toast.error('Erro ao excluir imagem!');
      }
    }
  }, [images, immobile.id_imovel, setLoading]);

  const toggleModal = useCallback(() => {
    setOpenModal(!openModal);
  }, [openModal]);

  const handleImageSelected = useCallback((itemSelected: any) => {
    setImageSelected(itemSelected);
    toggleModal();
  }, [toggleModal]);

  const handleFrontCover = useCallback(async (id: string) => {
    setLoading(true);

    try {
      const findIndexOfOldDestaque = images.findIndex((imageMapped) => imageMapped.destaque === 'SIM');
      const findIndexofNewDestaue = images.findIndex((imageMapped) => (
        imageMapped.id_imovel_imagem === id
      ));

      await api.put(`imovel/${immobile.id_imovel}/imagem/${id}`, {
        imagem: {
          destaque: 'SIM',
        },
      });

      if (findIndexofNewDestaue >= 0) {
        images[findIndexofNewDestaue] = {
          ...images[findIndexofNewDestaue],
          destaque: 'SIM',
        };
      }

      if (findIndexOfOldDestaque >= 0) {
        await api.put(`imovel/${immobile.id_imovel}/imagem/${images[findIndexOfOldDestaque].id_imovel_imagem}`, {
          imagem: {
            destaque: 'NAO',
          },
        });
        images[findIndexOfOldDestaque] = {
          ...images[findIndexOfOldDestaque],
          destaque: 'NAO',
        };
      }

      setImages([...images]);

      setLoading(false);
    } catch (err) {
      if (err.response) {
        setLoading(false);
        const { message } = handleErrors(err.response);
        toast.error(message[0]);
      } else {
        toast.error('Erro ao excluir imagem!');
      }
    }
  }, [images, immobile.id_imovel, setLoading]);

  return (
    <>
      {openModal
        && (
        <ModalImage
          toggleModal={toggleModal}
          imageSelected={imageSelected}
          immobileId={immobile.id_imovel}
          onHandleAddImage={setImages}
        />
        )}
      <div>
        <button
          type="button"
          className={styles.back}
          onClick={handleCloseUpdate}
        >
          <FiArrowLeft />
        </button>
        <button
          type="button"
          className={styles.addImage}
          onClick={() => { handleImageSelected({}); }}
        >
          <FiPlus />
          Adicionar
        </button>

      </div>

      <ul className={styles.list}>
        {images.length === 0
          ? (
            <li>
              <h1 className={styles.nenhumResultado}>Nenhum Resultado encontrado</h1>
            </li>
          )

          : images.map((imageMapped) => (
            <li key={imageMapped.id_imovel_imagem}>
              <img src={`http://mobi.tieng.com.br/imagens/${immobile.id_imovel}/${imageMapped.file}`} alt="Imovel" />
              <div className={styles.buttonContainer}>
                <button
                  className={imageMapped.destaque === 'SIM' ? styles.isDestaque : ''}
                  disabled={imageMapped.destaque === 'SIM'}
                  type="button"
                  onClick={() => { handleFrontCover(imageMapped.id_imovel_imagem); }}
                >
                  {imageMapped.destaque === 'SIM' ? 'Capa' : 'Destacar'}
                </button>
                <button type="button" onClick={() => { handleRemoveImage(imageMapped.id_imovel_imagem); }}>
                  Remover
                </button>
              </div>
            </li>
          ))}
      </ul>
    </>
  );
}
