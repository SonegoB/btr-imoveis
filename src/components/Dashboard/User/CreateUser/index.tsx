import { FormEvent, useCallback, useState } from 'react';
import { useLoading } from '@/hooks/useLoading';
import { api } from '@/services/axios';
import { toast } from 'react-toastify';

import styles from './styles.module.scss';

interface CreateUserProps {
    onHandleOptionSelected: (option: 'listar' | 'criar') => void;
    onHandleUpdateUser: (user: unknown) => void;
    users: unknown[];
}

export function CreateUser({
  onHandleOptionSelected,
  onHandleUpdateUser,
  users,
}: CreateUserProps): JSX.Element {
  const { setLoading } = useLoading();
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [confirmPassword, setConfirmPassword] = useState('');
  const [name, setName] = useState('');

  const handleCreateUser = useCallback(async (event: FormEvent) => {
    event.preventDefault();
    setLoading(true);
    try {
      if (password !== confirmPassword) {
        throw new Error('Campo "Senha" e "Confirmar senha" não contém os mesmos valores!');
      }

      const { data } = await api.post('/usuario', {
        usuario: {
          login: email,
          senha: password,
          nome: name,
        },
      });

      onHandleOptionSelected('listar');

      onHandleUpdateUser([...users, data.usuario]);

      toast.success('Usuario cadastrado com sucesso!');

      setLoading(false);
    } catch (err) {
      setLoading(false);
      if (err.message) {
        toast.error(err.message);
      }
    }
  }, [
    email, name, password, confirmPassword, setLoading,
    onHandleOptionSelected, onHandleUpdateUser, users,
  ]);

  return (
    <form className={styles.form} onSubmit={handleCreateUser}>
      <label htmlFor="email">E-mail</label>
      <input
        type="email"
        id="email"
        placeholder="Digite o E-mail"
        value={email}
        onChange={(e) => { setEmail(e.target.value); }}
      />

      <label htmlFor="nome">Nome</label>
      <input
        type="text"
        id="nome"
        placeholder="Digite o nome"
        value={name}
        onChange={(e) => { setName(e.target.value); }}
      />

      <label htmlFor="senha">Senha</label>
      <input
        type="password"
        id="senha"
        placeholder="Digite sua senha"
        value={password}
        onChange={(e) => { setPassword(e.target.value); }}
      />

      <label htmlFor="confirmar-senha">Confirmar senha</label>
      <input
        type="password"
        id="confirmar-senha"
        placeholder="Confirme a senha"
        value={confirmPassword}
        onChange={(e) => { setConfirmPassword(e.target.value); }}
      />

      <button type="submit">Cadastrar</button>
    </form>
  );
}
