import Head from 'next/head';

interface ISEOProps {
    title: string;
    // image?: string;
    shouldExcludeTitleSuffix?: boolean;
    shouldIndexPage?: boolean;
    pageImage?: string;
    description?: string;
}

export default function SEO({
  shouldExcludeTitleSuffix = false,
  shouldIndexPage = true,
  title,
  description = 'Amostragem de imóveis',
  pageImage,
}: ISEOProps): JSX.Element {
  const pageTitle = `${title} ${!shouldExcludeTitleSuffix ? '| Empresa de Prestação de Serviços' : ''}`;
  return (
    <Head>
      <title>{ pageTitle }</title>

      <meta name="description" content={description} />
      <meta name="image" content={pageImage || '/images/logo.png'} />

      {!shouldIndexPage && <meta name="robots" content="noindex,nofollow" />}

      <meta httpEquiv="x-ua-compatible" content="IE=edge,chrome=1" />
      <meta name="MobileOptimized" content="320" />
      <meta name="HandheldFriendly" content="True" />
      <meta name="theme-color" content="#121214" />
      <meta name="msapplication-TileColor" content="#121214" />
      <meta name="referrer" content="no-referrer-when-downgrade" />
      {/* Google nao fica tentando traduzir */}
      <meta name="google" content="notranslate" />

      {/* Facebook */}
      <meta property="og:title" content={pageTitle} />
      <meta property="og:description" content={description} />
      <meta property="og:locale" content="pt_BR" />
      <meta property="og:type" content="website" />
      <meta property="og:site_name" content={pageTitle} />
      <meta property="og:image" content={pageImage || '/images/logo.png'} />
      <meta property="og:image:secure_url" content={pageImage || '/images/logo.png'} />
      <meta property="og:image:alt" content="Thumbnail" />
      <meta property="og:image:type" content="image/png" />
      <meta property="og:image:width" content="1200" />
      <meta property="og:image:height" content="630" />

      {/* Twitter */}
      <meta name="twitter:title" content={pageTitle} />
      <meta name="twitter:card" content="summary_large_image" />
      <meta name="twitter:image" content="/images/logoMeta.png" />
      <meta name="twitter:image:src" content="/images/logoMeta.png" />
      <meta name="twitter:image:alt" content="Thumbnail" />
      <meta name="twitter:image:width" content="1200" />
      <meta name="twitter:image:height" content="620" />
    </Head>
  );
}
