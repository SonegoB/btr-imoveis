import { FiPhone, FiMapPin, FiMail } from 'react-icons/fi';
import { FaWhatsapp } from 'react-icons/fa';
import styles from './styles.module.scss';

export default function Footer(): JSX.Element {
  return (
    <footer className={styles.footerContainer}>
      <div className={styles.footerContent}>
        <strong>Fale conosco</strong>
        <p>
          <FiMapPin size={20} />
          {' '}
          <span>R. Teste, 0000 - Centro</span>
        </p>
        <p>
          <FaWhatsapp size={20} />
          {' '}
          <span>(00) 00000-0000</span>
        </p>
        <p>
          <FiMail size={20} />
          {' '}
          <span>email@email.com</span>
        </p>
      </div>
      <div className={styles.subFooter}>
        <span>Desenvolvido por: BTR Soluções Inovadoras</span>
      </div>
    </footer>
  );
}
