import React from 'react';
import { SRLWrapper } from 'simple-react-lightbox';
import Image from 'next/image';

type image = {
  id_imovel_imagem: string;
  file: string;
}

interface GaleryProps {
  images: image[],
  immobileId: string;
}

const options = {
  buttons: {
    backgroundColor: '#70a4aa',
    iconColor: '#F0F0F1',
    showThumbnailsButton: false,
    showAutoplayButton: false,
    showFullscreenButton: false,
  },
  settings: {
    overlayColor: 'rgba(0, 0, 0, 0.8)',
    transitionSpeed: 1000,
    transitionTimingFunction: 'linear',
  },
  thumbnails: {
    thumbnailsSize: ['120px', '100px'],
    thumbnailsOpacity: 0.4,
    thumbnailsGap: '4px',
  },
  caption: {
    captionColor: '#70a4aa',
  },
  progressBar: {
    size: '4px',
    backgroundColor: '#70a4aa',
    fillColor: '#70a4aa',
  },
};

export function Galery({ images, immobileId }: GaleryProps) {
  return (
    <SRLWrapper options={options}>
      {images.map((imageMapped) => (

        <a key={imageMapped.id_imovel_imagem} href={`http://mobi.tieng.com.br/imagens/${immobileId}/${imageMapped.file}`}>
          <Image
            src={`http://mobi.tieng.com.br/imagens/${immobileId}/${imageMapped.file}`}
            alt="Galeria"
            width={280}
            height={205}
          />
        </a>
      ))}
    </SRLWrapper>
  );
}
