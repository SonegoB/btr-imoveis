import React from 'react';
import SimpleReactLightbox from 'simple-react-lightbox';
import { AuthProvider } from './useAuth';
import { LoadingProvider } from './useLoading';
import { SidebarProvider } from './useSidebar';
import { SelectProvider } from './useSelect';

const AppProvider: React.FC = React.memo(({ children }) => (
  <AuthProvider>
    <LoadingProvider>
      <SimpleReactLightbox>
        <SelectProvider>
          <SidebarProvider>
            {children}
          </SidebarProvider>
        </SelectProvider>
      </SimpleReactLightbox>
    </LoadingProvider>
  </AuthProvider>
));

export default AppProvider;
