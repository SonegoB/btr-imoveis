import { Loading } from '@/components/Loading';
import {
  createContext, ReactNode, useContext, useState,
} from 'react';

interface LoadingContextData {
    loading: boolean;
    setLoading: (loading: boolean) => void;
}

interface LoadingProviderProps {
    children: ReactNode;
}

const LoadingContext = createContext<LoadingContextData>({} as LoadingContextData);

export function LoadingProvider({ children }: LoadingProviderProps): JSX.Element {
  const [loading, setLoading] = useState(false);

  return (
    <LoadingContext.Provider
      value={{ loading, setLoading }}
    >
      {children}
      {loading && <Loading />}
    </LoadingContext.Provider>
  );
}

export function useLoading(): LoadingContextData {
  const context = useContext(LoadingContext);

  return context;
}
