import Footer from '@/components/Footer';
import { Property } from '@/components/Property';
import {
  useCallback,
  useEffect, useState,
} from 'react';
import { Header } from '@/components/Header';
import SEO from '@/components/SEO';
import { GetServerSideProps } from 'next';
import { api } from '@/services/axios';
import formatCurrencyToBrazil from '@/utils/formatCurrencyToBrazil';
import { SearchImmobile } from '@/components/SearchImmobile';
import styles from './styles.module.scss';

interface ImoveisProps {
    immobile: unknown[];
    categories: ICategory[];
}

interface ICategory {
    id_categoria: string;
    titulo: string;
}

export default function Imoveis({
  immobile,
  categories,
}: ImoveisProps): JSX.Element {
  const [immobilesList, setImmobilesList] = useState<any[]>([]);

  useEffect(() => {
    setImmobilesList(immobile);
  }, [immobile]);

  const handleSearchToImmobile = useCallback((filteredImmobile: any) => {
    setImmobilesList(filteredImmobile);
  }, []);

  return (
    <>
      <SEO title="BTR Imóveis" />
      <Header type="page" />
      <main className={styles.container}>
        <div className={styles.banner}>
          <img src="/images/logo.png" alt="logo" />
        </div>
        <div className={styles.content}>
          <SearchImmobile
            onHandleSearchToImmobile={handleSearchToImmobile}
            categories={categories}
            type="page"
          />
        </div>
        <section className={styles.listProperties}>
          {immobilesList.length === 0 && <h1>Nenhum resultado encontrado</h1>}
          {immobilesList.map((immobileMapped) => (
            <Property
              key={immobileMapped.id_imovel}
              immobile={immobileMapped}
            />
          ))}
        </section>
      </main>
      <Footer />
    </>
  );
}

export const getServerSideProps: GetServerSideProps<ImoveisProps> = async () => {
  const response = await api.post('/imovel/pesquisa', {
    imovel: {
      status: 'ATIVO',
      id_modalidade_fk: '1',
    },
  });

  const immobile = response.data.result.map((immobileMapped) => ({
    ...immobileMapped,
    valorFormatado: formatCurrencyToBrazil(Number(immobileMapped.valor)),
    banner: immobileMapped.imagens.find((imageMapped) => imageMapped.destaque === 'SIM')
      ? immobileMapped.imagens.find((imageMapped) => imageMapped.destaque === 'SIM').file
      : immobileMapped.imagens[0]?.file ?? '',
  }));

  const categoryResponse = await api.post('/categoria/pesquisa', { categoria: { status: 'ATIVO' } });

  return {
    props: {
      immobile,
      categories: categoryResponse.data.result,
    },

  };
};
