import Footer from '@/components/Footer';
import { Galery } from '@/components/Galery';
import { Header } from '@/components/Header';
import SEO from '@/components/SEO';
import { api } from '@/services/axios';
import formatCurrencyToBrazil from '@/utils/formatCurrencyToBrazil';
import { GetStaticProps } from 'next';
import Link from 'next/link';
import React from 'react';
import { FiArrowLeft } from 'react-icons/fi';

import styles from './styles.module.scss';

interface IImmobile {
    id_imovel: string;
    titulo: string;
    descricao: string;
    valor: string;
    comodos: string;
    dimensao: string;
    garagem: string;
    coordenadas: string;
    link_maps: string;
    rua: string;
    numero: string;
    bairro: string;
    id_proprietario_fk: string;
    id_modalidade_fk: string;
    id_categoria_fk: string;
    id_cidade_fk: string;
    valorFormatado: string;
    categoria: string;
    tipo: string;
    cidade: string;
    uf: string;
    imagens: {
      id_imovel_imagem: string;
      file: string;
      destaque: 'SIM' | 'NAO'
    }[];
    banner: string;
}

interface DescricaoProps {
    immobile: IImmobile;
}

export default function Descricao({ immobile }: DescricaoProps): JSX.Element {
  return (
    <>
      <SEO
        title={`${immobile.titulo} | BTR Imóveis`}
        pageImage={`http://mobi.tieng.com.br/imagens/${immobile.id_imovel}/${immobile.banner}`}
        shouldExcludeTitleSuffix
        description={immobile.descricao}
      />
      <Header type="page" />
      <main className={styles.container}>
        <div className={styles.content}>
          <Link href="/">
            <a className={styles.back}><FiArrowLeft /></a>
          </Link>
          <h1>Descrição</h1>
          <p>
            Titulo:
            {' '}
            <span>{immobile.titulo}</span>
          </p>
          <p>
            Código:
            {' '}
            <span>
              #
              {immobile.id_imovel}
            </span>
          </p>
          <p>
            Tipo:
            {' '}
            <span>{immobile.tipo}</span>
          </p>
          <p>
            Valor:
            {' '}
            <span>{immobile.valorFormatado}</span>
          </p>
          <p>
            Dimensao:
            {' '}
            <span>
              {immobile.dimensao}
              m2
            </span>
          </p>
          <p>
            Link:
            {' '}
            <a href={immobile.link_maps} target="_blank" rel="noreferrer">Acessar mapa</a>
          </p>
          <p>
            Estado:
            {' '}
            <span>{immobile.uf}</span>
          </p>
          <p>
            Cidade:
            {' '}
            <span>{immobile.cidade}</span>
          </p>
          <p>
            Bairro:
            {' '}
            <span>{immobile.bairro}</span>
          </p>
          <p>
            Categoria:
            {' '}
            <span>{immobile.categoria}</span>
          </p>
          <p>
            Descrição:
            <span>{immobile.descricao}</span>
          </p>

          <h1 className={styles.img}> Imagens</h1>
          <div className={styles.imgContainer}>
            <Galery
              images={immobile.imagens}
              immobileId={immobile.id_imovel}
            />
          </div>
          <a
            href={`https://api.whatsapp.com/send?phone=+55035998913955&text=https://btr-imoveis.vercel.app/descricao/${immobile.id_imovel}`}
            target="_blank"
            rel="noopener noreferrer"
            className={styles.contato}
          >
            Compartilhe conosco
          </a>
        </div>
      </main>
      <Footer />
    </>
  );
}

export const getStaticPaths = () => ({
  paths: [],
  fallback: 'blocking',
});

export const getStaticProps: GetStaticProps = async ({ params }) => {
  const { id } = params;

  const response = await api.get(`/imovel/${id}`);
  let immobile = response.data.result[0];
  const modalityResponse = await api.get(`/modalidade/${immobile.id_modalidade_fk}`);
  const categoryResponse = await api.get(`/categoria/${immobile.id_categoria_fk}`);
  const cityResponse = await api.get(`/cidade/${immobile.id_cidade_fk}`);

  immobile = {
    ...immobile,
    valorFormatado: formatCurrencyToBrazil(Number(immobile.valor)),
    categoria: categoryResponse.data.titulo,
    tipo: modalityResponse.data.titulo,
    cidade: cityResponse.data.nome,
    uf: cityResponse.data.uf,
    banner: immobile.imagens.find((imageMapped) => imageMapped.destaque === 'SIM')
      ? immobile.imagens.find((imageMapped) => imageMapped.destaque === 'SIM').file
      : immobile.imagens[0]?.file ?? '',
  };

  return {
    props: {
      immobile,
    },
    revalidate: 30,
  };
};
